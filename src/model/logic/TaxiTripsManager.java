package model.logic;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.Cola;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.data_structures.Pila;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	private Lista<Servicio> listaServicios = new Lista<>();
	private Lista<Taxi> listaTaxis = new Lista<>();
	private Lista<Compania> listaComp= new Lista<>();
	private Lista<ServiciosValorPagado> listaValor= new Lista<>();
	private Lista<FechaServicios> listaFecha = new Lista<>();
	private Lista<CompaniaServicios> listaSerComp= new Lista<>();
	

	
	
	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		
		try 

		{

		//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json

		InputStream inputStream = new FileInputStream(direccionJson);



		//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));

		Gson gson = new GsonBuilder().create();

		reader.beginArray();

		while (reader.hasNext())

		{

		  Servicio servicio= gson.fromJson(reader, Servicio.class);

		  String tripStart= servicio.getTripStart();
		
		  
		 


		  if(tripStart!=null)

		{

			  String[] tripStar= tripStart.split("T");

			  String fechaInicio= tripStar[0];

			  String horaInicio= tripStar[1];
		
		
		  

		 

		  String[] tripEnd= servicio.getTripEnd().split("T");

		  String fechaFinal= tripEnd[0];

		  String horaFinal= tripEnd[1];

		  //System.out.println(fechaInicio);
		 

		  RangoFechaHora rango=  new RangoFechaHora(fechaInicio, fechaFinal, horaInicio, horaFinal);

		  servicio.setRango(rango);
		 //System.out.println(servicio.rango());
		  

		  
		//  Iterator iter= listaComp.iterator();
		 ///	  System.out.println(iter.next());
		 // }
		 

		  Taxi taxi= new Taxi();

		  taxi.setTaxiId(servicio.getTaxiId());

		 

		  CompaniaTaxi compTaxi= new CompaniaTaxi();

		  compTaxi.setNomCompania(servicio.getCompania());

		  compTaxi.setTaxi(taxi);
		  

		 

		  double ganancias= (servicio.getExtras()+servicio.getFare()+ servicio.getTips())- servicio.getTolls();

		  //distancia recorrida

		 

		  final int radioTierra= 6371;

		  double latLoc= servicio.getLatloc();

		  double latLocP= servicio.getLatlocP();

		  double lonLoc= servicio.getLonloc();

		  double lonLocP= servicio.getLonlocP();

		 

		 

		  double latDistance= Math.toRadians(latLoc-latLocP);

		  double lonDistance= Math.toRadians(lonLoc-lonLocP);

		  double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(latLocP)) * Math.cos(Math.toRadians(latLoc))

		 

		  * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

		 

		  double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		  double distance = radioTierra * c ;

		  double distanciaRecorridaTotal= Math.sqrt(Math.pow(distance, 2));

		 // System.out.println(distanciaRecorridaTotal);

		 

		  RangoDistancia rangoDis= new RangoDistancia();

		  rangoDis.setLimiteSuperior(distanciaRecorridaTotal);

		 

		  InfoTaxiRango infoTaxi= new InfoTaxiRango();

		  infoTaxi.setCompany(servicio.getCompania());
		 // System.out.println(infoTaxi.getCompany());
		  infoTaxi.setIdTaxi(servicio.getTaxiId());

		  infoTaxi.setPlataGanada(ganancias);

		  infoTaxi.setRango(rango);

		  infoTaxi.setTiempoTotal(Integer.toString(servicio.getTripSeconds()));

		  infoTaxi.setDistanciaTotalRecorrida(distanciaRecorridaTotal);
		 

		  Lista<Servicio> listaServicioRango= new Lista<>();

		  Lista<Servicio> listaServiciosTaxi= new Lista<>();

		 

		  if(servicio.rango().equals(rango) && servicio.getTaxiId().equalsIgnoreCase(taxi.getTaxiId()))

		  {

		  listaServicioRango.agregarElementoFinal(servicio);

		  }

		 


		  listaServiciosTaxi.agregarElementoFinal(servicio);
		  infoTaxi.setServiciosPrestadosEnRango(listaServicioRango);
		//  Iterator iter = infoTaxi.getServiciosPrestadosEnRango().iterator();
		//  while(iter.hasNext())
			  
		//  {
			  
		//	  System.out.println(iter.next());
		//  }
		  
		  taxi.setServiciosTaxi(listaServiciosTaxi);
		  //System.out.println(taxi.getServiciosTaxi());

		  taxi.setInfoTaxi(infoTaxi);

		  taxi.setCompany(compTaxi);
		  
		  listaServicios.agregarElementoFinal(servicio);
		

		  double valorAc=0.0;
		  ServiciosValorPagado servValor= new ServiciosValorPagado();
		  valorAc += ganancias;
		  servValor.setValorAcumulado(valorAc);
		  servValor.setServiciosAsociados(listaServiciosTaxi);
		  taxi.setServiciosvalor(servValor);
		  
		  listaTaxis.agregarElementoFinal(taxi);
		  

		  Compania compania= new Compania();
		  compania.setNombre(servicio.getCompania());
		  Lista<Taxi> taxisComp= new Lista<>();
		  taxisComp.agregarElementoFinal(taxi);
		  compania.setTaxisInscritos(taxisComp);
		  
		  listaComp.agregarElementoFinal(compania);
		  
		  CompaniaServicios compser= new CompaniaServicios();
		  compser.setNomCompania(servicio.getCompania());
		  compser.setRango(rango);
		  compser.setServicios(listaServicioRango);
		  compser.setServicios(listaServiciosTaxi);
		  listaSerComp.agregarElementoFinal(compser);
		  
		  //System.out.println(infoTaxi.getServiciosPrestadosEnRango());
		}  

		 

		} 


		reader.close();


		}	catch (UnsupportedEncodingException ex) 

		{

		System.out.println(ex.getMessage());

		}

		catch (IOException ex) 

		{

		System.out.println(ex.getMessage());

		} catch(IllegalStateException ex)
		{
			
		}

		return true;

		}




			
		
	
	@Override //1A
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		
		Cola<Servicio> colaRetorno= new Cola<>();
		Lista<Servicio> listaserv= new Lista<>();
	
		for(int i=0; i<listaServicios.darNumeroElementos();i++)
		{
			Servicio actual= listaServicios.darElemento(i);
			SimpleDateFormat fr= new SimpleDateFormat("yyyy-mm-dd");
			try {
				Date datei= fr.parse(actual.rango().getFechaInicial());
				Date datef= fr.parse(actual.rango().getFechaFinal());
				
				//System.out.println(datei);
				Date dater= fr.parse(rango.getFechaInicial());
				Date daterf= fr.parse(rango.getFechaFinal());
				
				if(datei.compareTo(dater)>=0 && datef.compareTo(daterf)<=0)
				{
					listaserv.agregarElementoFinal(actual);
				}
			
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
				//System.out.println(actual);
		}
		
		
	
		for(int j=1;j<listaserv.darNumeroElementos();j++)
		{
			for(int i=j;i>0  ;i--)
			{
				if(listaserv.darElemento(i).rango().getHoraInicio().compareTo(listaserv.darElemento(j).rango().getHoraInicio())<0)
				{
					listaserv.swap(listaserv.darElemento(i),listaserv.darElemento(j));
				}
			}
		}
		
		for(int i=0;i<listaserv.darNumeroElementos();i++)
		{
			colaRetorno.enqueue(listaserv.darElemento(i));
		}
					

			return colaRetorno;
		}
	
	
	

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
			
			Taxi masServicio= new Taxi();
			int servmayor=0;
			int servActual=0;
			
			for(int i=0; i<listaTaxis.darNumeroElementos();i++)
			{
				Taxi actual= listaTaxis.darElemento(i);
				Lista<Servicio> servicios= actual.getServiciosTaxi();
				
				for(int j=0; j<servicios.darNumeroElementos();j++)
				{
					Servicio actualser= servicios.darElemento(i);
					if(actualser.getCompania().equals(company))
					{
						if(actualser.rango().equals(rango))
						{
							servActual= servicios.darNumeroElementos();
						}
					}
					
					if(servActual>servmayor)
					{
						servmayor= servActual;
						masServicio= actual;
					}
				}
				
			
		
			}
		return masServicio;
			}
		
	
	

	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		InfoTaxiRango infoTaxi= new InfoTaxiRango();
		
		for(int i=0; i< listaTaxis.darNumeroElementos();i++)
		{
			Taxi actual= listaTaxis.darElemento(i);
			if(actual.getTaxiId().equalsIgnoreCase(id) )
			{
				//System.out.println(actual);
				Lista<Servicio> serviciosTaxi= actual.getServiciosTaxi();
				
				Iterator iter= serviciosTaxi.iterator();
				if(serviciosTaxi.darNumeroElementos()>0)
				{
					
				while(iter.hasNext())
				{
					Servicio servicioActual= (Servicio) iter.next();
					//System.out.println(servicioActual);
					
					SimpleDateFormat fr= new SimpleDateFormat("yyyy-mm-dd");
					try {
						Date datei= fr.parse(servicioActual.rango().getFechaInicial());
						Date datef= fr.parse(servicioActual.rango().getFechaFinal());
						
						//System.out.println(datei);
						Date dater= fr.parse(rango.getFechaInicial());
						Date daterf= fr.parse(rango.getFechaFinal());
						
						if(datei.compareTo(dater)>=0 && datef.compareTo(daterf)<=0)
						{
							infoTaxi= actual.getInfoTaxi();
						}
					}
				
					catch(Exception e)
					{
						
					}
				}
					//if(servicioActual.rango().getHoraInicio().equals(rango.getHoraInicio()) && servicioActual.rango().getHoraFinal().equals(rango.getHoraFinal()) )
					//{
					//}
				}
				}
			}
		
		return infoTaxi;
	}
	
	

	@Override //4A
	public Lista<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		Lista<RangoDistancia> listaRetorno= new Lista<>();
		Pila<RangoDistancia> pila= new Pila<>();
		Lista <Servicio> serviciosDis= new Lista<>();
		SimpleDateFormat fr= new SimpleDateFormat("hh:mm:ss.sss");
		
			for(int j=0;j<listaServicios.darNumeroElementos();j++)
			{
				RangoDistancia rangoActual= new RangoDistancia();
				Servicio serActual= listaServicios.darElemento(j);
				//System.out.println(serActual);
				double millasMas=0.0;
				try {
					
					Date horai= fr.parse(serActual.rango().getHoraInicio());
					Date horaf= fr.parse(serActual.rango().getHoraFinal());
					//System.out.println(horai);
					Date horair= fr.parse(horaInicial);
					Date horafr= fr.parse(horaFinal);
					
					if(serActual.rango().getFechaInicial().compareTo(fecha)>=0 )
					{
						if(horai.compareTo(horair)>=0 && horaf.compareTo(horafr)<=0)
						//if(serActual.rango().getFechaInicial().equals(fecha) && serActual.rango().getHoraInicio().equals(horaInicial) && serActual.rango().getHoraFinal().equals(horaFinal)){
						
					serviciosDis.agregarElementoFinal(serActual);
					rangoActual= serActual.getRangoDis();
					System.out.println(serActual);
					
					
					if(serActual.getTripMiles() > millasMas)
					{
					rangoActual.setServiciosEnRango(serviciosDis);
					//pila.push(rangoActual);
					
					}
					}}
				 catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				listaRetorno.agregarElementoFinal(rangoActual); 
				
				}
			
			
			
			
			return listaRetorno;
		}
		
	

	@Override //1B
	public Lista<Compania> darCompaniasTaxisInscritos() 
	{
	Lista <Compania> companias = new Lista<>();
	Pila<Compania> pila= new Pila<>();

	for(int i=0; i<listaServicios.darNumeroElementos();i++)
	{
	Servicio actual= listaServicios.darElemento(i);

	String x =actual.getCompania();

	for(int j= companias.darNumeroElementos();j>0;j--){
	if(x.compareToIgnoreCase(companias.darElemento(j).getNombre())>0){
	pila.push(companias.darElemento(j));
	while(j>0){
	Compania temp= companias.eliminarElemento(j);
	pila.push(temp);
	j--;
	}
	}
	else{
	Compania temp= companias.eliminarElemento(j);
	pila.push(temp);


	}


	}
	while(pila!= null){
	Compania m= pila.pop();
	companias.agregarElementoFinal(m);
	}


	}




	return companias;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
	Taxi mayorfacturacion= new Taxi();
	int mayor=0;

	for(int i=0; i<listaTaxis.darNumeroElementos();i++)
	{
	Taxi actual= listaTaxis.darElemento(i);
	Lista<Servicio> servicios= actual.getServiciosTaxi();
	int facturacion =0;
	for(int j=0; j<servicios.darNumeroElementos();j++)
	{
	Servicio actualser= servicios.darElemento(i);
	if(actualser.getCompania().equals(nomCompania))
	{
	if(actualser.rango().equals(rango))
	{
	facturacion+= actualser.getFare()  ;
	}
	}

	if(facturacion>mayor)
	{
	mayor= facturacion;
	mayorfacturacion= actual;
	}
	if(j == servicios.darNumeroElementos()-1)
	{
	facturacion =0;
	}
	}

	}


	return mayorfacturacion ;
	}


	@Override //3B
	public ServiciosValorPagado darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{

	ServiciosValorPagado retornoInfo= new ServiciosValorPagado();

	for(int i=0;i<listaValor.darNumeroElementos();i++)
	{
	ServiciosValorPagado Valor= listaValor.darElemento(i);
	if(Valor.getIdZona().equalsIgnoreCase(idZona))
	{
	for(int j=0;j<Valor.getServiciosAsociados().darNumeroElementos();j++)
	{
	Servicio actual= Valor.getServiciosAsociados().darElemento(j);

	if(actual.rango().equals(rango))
	{
	retornoInfo= Valor;
	}
	}
	}

	}
	return retornoInfo;
	}


	@Override //4B
	public Lista<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //2C
	public Lista<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
	Cola<CompaniaServicios> cola= new Cola<>();
	Lista <CompaniaServicios> companias = new Lista<>();
	for(int i=0;i<listaSerComp.darNumeroElementos();i++)
	{
	CompaniaServicios Com= listaSerComp.darElemento(i);
	if(Com.getRango().equals(rango))
	{
	for(int j= companias.darNumeroElementos();j>0;j--){
	if(Com.getServiciosRango()>companias.darElemento(j).getServiciosRango()){
	cola.enqueue(Com);
	while(j>0){
	CompaniaServicios temp= companias.eliminarElemento(j);
	cola.enqueue(temp);
	j--;
	}
	}
	else{
	CompaniaServicios temp= companias.eliminarElemento(j);
	cola.enqueue(temp);


	}


	}
	while(cola!= null && companias.darNumeroElementos()<= n){
	CompaniaServicios m= cola.dequeue();
	companias.agregarElementoFinal(m);
	}

	}

	}
	return companias;
	}

	@Override //3C
	public Lista<CompaniaTaxi> taxisMasRentables()
	{
		Lista<CompaniaTaxi> listacompania= new Lista<>();
		double mayorganancia= 0.0;
		CompaniaTaxi compTaxiMayor= new CompaniaTaxi();
		
		for(int i=0;i<listaComp.darNumeroElementos();i++)
		{
			Compania compania= listaComp.darElemento(i);
			if(compania.getTaxisInscritos().darNumeroElementos()>0)
			{
				Iterator<Taxi> iter= compania.getTaxisInscritos().iterator();
				
				while(iter.hasNext())
				{
					double taxiganancia=0.0;
					double ganancia=0.0;
					Taxi taxi= iter.next();
					taxiganancia= taxi.getInfoTaxi().getPlataGanada();
					double taxidistancia= taxi.getInfoTaxi().getDistanciaTotalRecorrida();
					ganancia= taxiganancia/ taxidistancia;
					
					if(ganancia>mayorganancia)
					{
						mayorganancia= ganancia;
						compTaxiMayor= taxi.getCompany();
					}
				}
			}
			listacompania.agregarElementoFinal(compTaxiMayor);
		}
		
		
		return listacompania;
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	

  
}
