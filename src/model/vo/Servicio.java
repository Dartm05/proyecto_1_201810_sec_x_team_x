package model.vo;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	

	private String trip_id;
	private String taxi_id;
	private String company;
	private RangoDistancia rangoDis;
	
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;
	//time
	private String trip_end_timestamp;
	private String trip_start_timestamp;
	private RangoFechaHora rangoF;
	
	//tract
	private String dropoff_census_Tract;
	private String pickup_census_Tract;
	
	
	private String payment_type;
	
	
	private double dropoff_centroid_latitude;
	private double dropoff_centroid_longitude;
	private double pickup_centroid_latitude;
	private double pickup_centroid_longitude;

	//community
	private int dropoff_community_area;
	private int pickup_community_area;
	
	//fares
	private double tolls;
	private double fare;
	private double extras;
	private double tips;
	
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return trip_id;
	}	

	public void setTripId(String id) {
		this.trip_id= id;
	}	
	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}	
	
	
	public void setTaxiId(String id) {
		this.taxi_id=id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		 return trip_seconds;
	}
	
	
	public void setTripSeconds(int sec) {
		this.trip_seconds=sec;
	}
	

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return trip_miles;
	}
	
	
	public void setTripMiles(double miles) {
		this.trip_miles= miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return trip_total;
	}

	
	public void setTripTotal(double tot) {
		this.trip_total=tot;
	}

	@Override
	public int compareTo(Servicio o) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public RangoFechaHora rango()
	{
		return rangoF;
	}
	
	
	public void setRango(RangoFechaHora rangoT)
	{
		this.rangoF= rangoT;
	}
	
	

	public String getDropTract() {
		return dropoff_census_Tract;
	}

	public void setDropTract(String dropTract) {
		this.dropoff_census_Tract = dropTract;
	}

	public String getPickTract() {
		return pickup_census_Tract;
	}

	public void setPickTract(String pickTract) {
		this.pickup_census_Tract = pickTract;
	}


	public double getTolls() {
		return tolls;
	}

	public void setTolls(int tolls) {
		this.tolls = tolls;
	}

	public double getFare() {
		return fare;
	}

	public void setFare(double fare2) {
		this.fare = fare2;
	}

	public int getPickcom() {
		return pickup_community_area;
	}

	public void setPickcom(int pickcom) {
		this.pickup_community_area = pickcom;
	}

	public int getDropcom() {
		return dropoff_community_area;
	}

	public void setDropcom(int dropcom) {
		this.dropoff_community_area= dropcom;
	}

	public double getExtras() {
		return extras;
	}

	public void setExtras(double extras2) {
		this.extras = extras2;
	}



	public double getTips() {
		return tips;
	}

	public void setTips(int tips) {
		this.tips = tips;
	}

	public String getPayment() {
		return payment_type;
	}

	public void setPayment(String payment) {
		this.payment_type = payment;
	}

	

	


	public double getLatloc() {
		return dropoff_centroid_latitude;
	}

	public void setLatloc(double latloc) {
		this.dropoff_centroid_latitude = latloc;
	}

	public double getLonloc() {
		return dropoff_centroid_longitude;
	}

	public void setLonloc(double lonloc) {
		this.dropoff_centroid_longitude = lonloc;
	}

	public double getLatlocP() {
		return pickup_centroid_latitude;
	}

	public void setLatlocP(double latlocP) {
		this.pickup_centroid_latitude = latlocP;
	}

	public double getLonlocP() {
		return pickup_centroid_longitude;
	}

	public void setLonlocP(double lonlocP) {
		this.pickup_centroid_longitude= lonlocP;
	}

	public String getCompania() {
		return company;
	}

	public void setCompania(String compania) {
		this.company = compania;
	}

	public RangoDistancia getRangoDis() {
		return rangoDis;
	}

	public void setRangoDis(RangoDistancia rangoDis) {
		this.rangoDis = rangoDis;
	}

	public String getTripEnd() {
		return trip_end_timestamp;
	}

	public void setTripEnd(String tripEnd) {
		this.trip_end_timestamp = tripEnd;
	}

	public String getTripStart() {
		return trip_start_timestamp;
	}

	public void setTripStart(String tripStart) {
		this.trip_start_timestamp = tripStart;
	}
}
