package model.vo;


import model.data_structures.Lista;

public class CompaniaServicios implements Comparable<CompaniaServicios> {
private String nomCompania;
private int serviciosRango;
private RangoFechaHora rango;
public RangoFechaHora getRango()
{
return rango;
}
public void setRango(RangoFechaHora rango)
{
this.rango = rango;
}
private Lista<Servicio> servicios;

public String getNomCompania() {
return nomCompania;
}
public int getServiciosRango() {
return serviciosRango;
}
public void setNomCompania(String nomCompania) {
this.nomCompania = nomCompania;
}
public void setserviciosRango(RangoFechaHora rango) {
int x =0;
for(int i=0;i<servicios.darNumeroElementos();i++)
if(servicios.darElemento(i).rango().equals(rango)){
x++;
}
this.serviciosRango = x;
}

public Lista<Servicio> getServicios() {
return servicios;
}

public void setServicios(Lista<Servicio> listaServicios) {
this.servicios = listaServicios;
}

@Override
public int compareTo(CompaniaServicios o) {
// TODO Auto-generated method stub
return 0;
}

}