package model.vo;

import model.data_structures.Lista;

public class Compania implements Comparable<Compania> {
	
	private String nombre;
	
	private Lista<Taxi> taxisInscritos;	
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Lista<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void setTaxisInscritos(Lista<Taxi> listaTaxis) {
		this.taxisInscritos = listaTaxis;
	}

	@Override
	public int compareTo(Compania o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	

}
