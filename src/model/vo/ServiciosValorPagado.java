package model.vo;


import model.data_structures.Lista;

public class ServiciosValorPagado implements Comparable<ServiciosValorPagado> {

private String idTaxi;

private String idZona;

private RangoFechaHora rango;

private Lista<Servicio> serviciosAsociados;

private double valorAcumulado;

/**
* modela los servicios que se recogieron en la zonade consulta y terminaron en otra zona en el rango dado.
*/
private int numServicios1;
/**
* modela los servicios que se recogieron en otra zona y terminaron en la zona de consulta en el rango dado.
*/
private int numServicios2;
/**
* modela los serviciosque se recogieron en la zona de consulta y terminaron en la zona de consulta en el rango dado.
*/
private int numServicios3;
/**
* modela el valor acumulado de los servicios que se recogieron en la zonade consulta y terminaron en otra zona en el rango dado.
*/
private double valorAcumulado1;
/**
* modela el valor acumulado de los servicios que se recogieron en otra zona y terminaron en la zona de consulta en el rango dado.
*/
private double valorAcumulado2;
/**
* modela el valor acumulado de los serviciosque se recogieron en la zona de consulta y terminaron en la zona de consulta en el rango dado.
*/
private double valorAcumulado3;

public String getIdTaxi()
{
return idTaxi;
}


public void setIdTaxi(String idTaxi)
{
this.idTaxi = idTaxi;
}
public String getIdZona()
{
return idZona;
}
public void setIdZona(String idZona)
{
this.idZona = idZona;
}

public RangoFechaHora getRango()
{
return rango;
}


public void setRango(RangoFechaHora rango)
{
this.rango = rango;
}

public Lista<Servicio> getServiciosAsociados() {
return serviciosAsociados;
}

public void setServiciosAsociados(Lista<Servicio> serviciosAsociados) {
this.serviciosAsociados = serviciosAsociados;
}
public int getnumServicios1() {
return numServicios1;
}

public void setnumServicios1(int numServicios) {
this.valorAcumulado1 = numServicios;
}
public int getnumServicios2() {
return numServicios2;
}

public void setnumServicios2(int numServicios) {
this.valorAcumulado2 = numServicios;
}
public int getnumServicios3() {
return numServicios3;
}

public void setnumServicios3(int numServicios) {
this.valorAcumulado3 = numServicios;
}

public double getValorAcumulado1() {
return valorAcumulado1;
}

public void setValorAcumulado1(double valorAcumulado) {
this.valorAcumulado1 = valorAcumulado;
}
public double getValorAcumulado2() {
return valorAcumulado2;
}

public void setValorAcumulado2(double valorAcumulado) {
this.valorAcumulado2 = valorAcumulado;
}

public double getValorAcumulado3() {
return valorAcumulado3;
}

public void setValorAcumulado3(double valorAcumulado) {
this.valorAcumulado3 = valorAcumulado;
}


@Override
public int compareTo(ServiciosValorPagado o) {
	// TODO Auto-generated method stub
	return 0;
}


public double getValorAcumulado() {
	return valorAcumulado;
}


public void setValorAcumulado(double valorAcumulado) {
	this.valorAcumulado = valorAcumulado;
}



}
