package model.data_structures;

import java.awt.List;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Iterator;





 // Queue //
public class ListaDobleEncadenada<K> implements ILista<K> 
{
	// ATRIBUTOS //
	private NodoDoble<K> primero;
	
	private NodoDoble<K> ultimo;
	
	private NodoDoble<K> actual;
	
	private int cantidad;
	
	private int posActual;
	
	// CONSTRUCTOR //
	
	public ListaDobleEncadenada( )
	{
		primero= null;
		ultimo= null;
		actual= primero;
		cantidad= 0;
		posActual= -1;
	}

	// MÉTODOS //
	
	  
	public Iterator<K> iterator( ) 
	{
		// TODO Auto-generated method stub
		
		return new Iterator<K>( ) 
		{
			private NodoDoble<K> aux= primero;
						
			  
			public boolean hasNext( ) 
			{
				return aux!= null;
			}
						
			  
			public K next( ) 
			{
				K item= aux.darItem( );
				aux= aux.darSiguiente( );
				return item;
			}
			
			public boolean hasPrevious( )
			{
				return (posActual > 0);
			}
			
			  
		    public void remove() 
			{
											
			}
		}	;
	}

	  
	public void agregarElementoFinal(K elem) 
	{
		// TODO Auto-generated method stub
		
		if (cantidad== 0)
		{
			NodoDoble<K> primer= new NodoDoble( null,null, elem );
			primero= primer;
			ultimo= primer;
			actual= primer;
			cantidad++;
		}
		else
		{
			NodoDoble<K> nuevo= new NodoDoble( ultimo, null, elem );
			ultimo.cambiarSiguiente(nuevo);
			ultimo= nuevo;
			cantidad++;
		}
	}

	  
	public K darElemento(int pos) 
	{
		// TODO Auto-generated method stub
		if(pos<0 || pos>=cantidad)
			throw new ArrayIndexOutOfBoundsException();
		
		if(primero==null){
			throw new ArrayIndexOutOfBoundsException();
		}
		
		NodoDoble<K> actual= primero;
		
		
		int contador= 0;
		
		if( pos ==0)
		{
			return actual.darItem( );
		}
		while( actual.darSiguiente()!= null)
		{
			
			actual= actual.darSiguiente( );
			contador++;
			if(contador==pos){
				return actual.darItem();
			}
		}
		return null;
	}


	  
	public int darNumeroElementos( ) 
	{
		// TODO Auto-generated method stub
		return cantidad;
	}

	  
	public K darElementoPosicionActual( ) 
	{
		// TODO Auto-generated method stub
		return actual.darItem( );
	}

	  
	public boolean avanzarSiguientePosicion( ) 
	{

		// TODO Auto-generated method stub
		boolean avanza= false;
		if( actual.darSiguiente()!= null && actual!= null)
		{
			actual= actual.darSiguiente();
			avanza= true;
		}
		return avanza;	
	}

	  
	public boolean retrocederPosicionAnterior( ) 
	{
		// TODO Auto-generated method stub
		boolean retrocede= false;
		if( actual!= null && actual.darAnterior()!= null)
		{
			actual= actual.darAnterior();
			retrocede= true;
		}
		return retrocede;
	}
	
	
	public NodoDoble<K> darPrimero()
	{
		return primero;
	}

	
	public K darElementoALinicio()
	{
		return primero.darItem();
	}
	
	
	public K darElementoAlfinal()
	{
		return ultimo.darItem();
	}
	
	
	public void agregarElementoAlInicio(K elem)
	{
		
		NodoDoble<K> primer= new NodoDoble( null,null, elem );
		
		primero.cambiarAnterior(primer);
		primer.cambiarSiguiente(primero);
		primero= primer;
		cantidad++;
	}

	public void eliminarUltimoElemento() {
		if(primero==null){
			throw new EmptyStackException();
		}
		NodoDoble<K> anterior=ultimo.darAnterior();
		ultimo.cambiarAnterior(null);
		ultimo=anterior;
		
		if(ultimo!=null)
			ultimo.cambiarSiguiente(null);
		
		cantidad--;
		
	}

	public void eliminarPrimerElemento() {
		if(primero==null){
			throw new EmptyStackException();
		}
		NodoDoble<K> siguiente=primero.darSiguiente();
		primero.cambiarSiguiente(null);
		if(siguiente!=null)
			siguiente.cambiarAnterior(null);
		
		primero=siguiente;
		
		cantidad--;
	}

	@Override
	public K eliminarElemento(int pos) {


		if (primero == null)
			throw new NullPointerException();
		else {
			NodoDoble<K> current = primero;
			for (int newPos = 0; newPos <= pos - 1; newPos++)
				current = current.next;
			K rta = current.next.darItem();
			current.next = current.next.next;
			cantidad--;
			return rta;
		}
	}
	
	
}
