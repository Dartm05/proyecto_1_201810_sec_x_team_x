package model.data_structures;

public class NodoDoble <K>
{
	// ATRIBUTOS//
	
	private NodoDoble<K> anterior;
	
	private NodoDoble<K> siguiente;
	
	private K dato;

	public NodoDoble<K> next;
	
	//CONSTRUCTOR//
	
	public NodoDoble (NodoDoble<K> ant, NodoDoble<K> sig, K pDato)
	{
		anterior= ant;
		siguiente= sig;
		dato= pDato;
	}
	
	//MÉTODOS//
	
	public NodoDoble<K> darAnterior( )
	{
		return anterior;
	}
	
	public NodoDoble<K> darSiguiente( )
	{
		return siguiente;
	}
	
	public K darItem( )
	{
		return dato;
	}
	
	public void cambiarAnterior( NodoDoble<K> ant)
	{
		anterior= ant;
	}
	
	public void cambiarSiguiente( NodoDoble<K> sig)
	{
		siguiente= sig;
	}
}
