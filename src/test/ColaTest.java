package test;

import model.data_structures.Cola;
import junit.framework.TestCase;


public class ColaTest<K> extends TestCase {

	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	Cola<String> cola = new Cola<String>();

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	public void setupEscenario1( )
	{
		cola.enqueue("String test 1");
		cola.enqueue("String test 2");
		cola.enqueue("String test 3");
		cola.enqueue("String test 4");
		cola.enqueue("String test 5");
		cola.enqueue("String test 6");
		cola.enqueue("String test 7");
		
	}
	
	public void setupEscenario2( )
	{
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
	}

	public void testEnqueue(){
		
		cola.enqueue("String test 1");
		
		cola.enqueue("String test 2");
		
		cola.enqueue("String test 3");
		
		cola.enqueue("String test 4");
		
		cola.enqueue("String test 5");
		
		cola.enqueue("String test 6");
		
		cola.enqueue("String test 7");
		
	}
 
	public void testDequeue(){
		
		setupEscenario1( );

		assertEquals("Los elementos no tienen el orden que se supone", "String test 1", cola.dequeue());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 2", cola.dequeue());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 3", cola.dequeue());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 4", cola.dequeue());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 5", cola.dequeue());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 6", cola.dequeue());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 7", cola.dequeue());
	}

	public void testDarNumeroDeElementos(){
		
		assertEquals("Al principio no hay elementos", 0, cola.size());
		
		setupEscenario1( );

		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 7, cola.size());
		
		cola.enqueue("String test 8");
		
		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 8, cola.size());
	}

	public void testIsEmpty(){

		assertTrue("Al principio la cola debe estar vacia", cola.isEmpty());
		
		setupEscenario1( );
		
		assertFalse("Ahora la cola no debe estar vacia", cola.isEmpty());
		
		setupEscenario2( );
		
		assertTrue("Ahora la cola debe estar vacia", cola.isEmpty());
	}

}

