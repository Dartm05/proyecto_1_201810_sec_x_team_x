package test;

import model.data_structures.Lista;
import junit.framework.TestCase;


public class ListaTest<T> extends TestCase {

	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------


	//	private ListaEncadenada<T> listaEncadenada;
	Lista<String> lista = new Lista<String>();

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	public void setupEscenario1( )
	{

		lista.agregarElementoFinal("String test 1");
		lista.agregarElementoFinal("String test 2");
		lista.agregarElementoFinal("String test 3");
		lista.agregarElementoFinal("String test 4");
		lista.agregarElementoFinal("String test 5");
		lista.agregarElementoFinal("String test 6");
		lista.agregarElementoFinal("String test 7");

	}

	public void testAgregarElementos(){
		lista.agregarElementoFinal("String test 1");
		
		lista.agregarElementoFinal("String test 2");
		
		lista.agregarElementoFinal("String test 3");
		
		lista.agregarElementoFinal("String test 4");
		
		lista.agregarElementoFinal("String test 5");
		
		lista.agregarElementoFinal("String test 6");
		
		lista.agregarElementoFinal("String test 7");

		assertNotNull( "El elemento no se cre�", lista.darElemento(0) );
		
		assertNotNull( "El elemento no se cre�", lista.darElemento(2) );
		
		assertNotNull( "El elemento no se cre�", lista.darElemento(4) );
		
		assertNotNull( "El elemento no se cre�", lista.darElemento(6) );
	}

	public void testDarElementos(){
		setupEscenario1( );

		assertEquals("No se agreg� el elemento correctamente", "String test 1", lista.darElemento(0));
		
		assertEquals("No se agreg� el elemento correctamente", "String test 3", lista.darElemento(2));
		
		assertEquals("No se agreg� el elemento correctamente", "String test 5", lista.darElemento(4));
		
		assertEquals("No se agreg� el elemento correctamente", "String test 7", lista.darElemento(6));
		
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 1", lista.darElemento(0));
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 2", lista.darElemento(1));
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 3", lista.darElemento(2));
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 4", lista.darElemento(3));
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 5", lista.darElemento(4));
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 6", lista.darElemento(5));
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 7", lista.darElemento(6));
	}

	public void testDarNumeroDeElementos(){

		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 0, lista.darNumeroElementos());
		
		setupEscenario1( );

		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 7, lista.darNumeroElementos());
	
		lista.agregarElementoFinal("String test 8");
		
		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 8, lista.darNumeroElementos());

	}

	public void testEliminarPrimero(){

		lista.agregarElementoFinal("String test 0");
		
		lista.eliminarPrimero();
		
		assertEquals("No se elimino el primero", 0, lista.darNumeroElementos());
		
		
		setupEscenario1( );

		lista.eliminarPrimero();
		
		lista.eliminarPrimero();
		
		lista.eliminarPrimero();
		
		assertEquals("No se elimino el primero", 4, lista.darNumeroElementos());
		
		assertEquals("No se elimino el primero", "String test 4", lista.darElemento(0));
	}
	
	public void testDarUltimoElemento(){

		lista.agregarElementoFinal("String test 0");
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 0", lista.darUltimoElemento());
		
		lista.eliminarPrimero();
		
		
		setupEscenario1( );

		assertEquals("Los elementos no tienen el orden que se supone", "String test 7", lista.darUltimoElemento());
		
	}
	
	public void testEliminarElemento(){

		setupEscenario1( );

		lista.eliminarElemento(0);
		
		lista.eliminarElemento(0);
		
		lista.eliminarElemento(0);
		
		assertEquals("Los elementos no tienen el orden que se supone", 4, lista.darNumeroElementos());
		
	}
}
