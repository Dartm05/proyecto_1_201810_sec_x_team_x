package test;

import model.data_structures.Pila;
import junit.framework.TestCase;


public class PilaTest<T> extends TestCase {

	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	Pila<String> pila = new Pila<String>();

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	public void setupEscenario1( )
	{
		pila.push("String test 1");
		pila.push("String test 2");
		pila.push("String test 3");
		pila.push("String test 4");
		pila.push("String test 5");
		pila.push("String test 6");
		pila.push("String test 7");
	}

	public void setupEscenario2( )
	{
		pila.pop();
		pila.pop();
		pila.pop();
		pila.pop();
		pila.pop();
		pila.pop();
		pila.pop();
	}
	
	public void testPush(){
		
		pila.push("String test 1");
		
		pila.push("String test 2");
		
		pila.push("String test 3");
		
		pila.push("String test 4");
		
		pila.push("String test 5");
		
		pila.push("String test 6");
		
		pila.push("String test 7");
	}

	public void testPop(){
		
		setupEscenario1( );

		assertEquals("Los elementos no tienen el orden que se supone", "String test 7", pila.pop());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 6", pila.pop());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 5", pila.pop());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 4", pila.pop());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 3", pila.pop());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 2", pila.pop());
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 1", pila.pop());
	}

	public void testSize(){
		
		assertEquals("Al principio no hay elementos", 0, pila.size());
		
		setupEscenario1( );

		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 7, pila.size());
		
		pila.push("String test 8");
		
		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 8, pila.size());
	}

	public void testIsEmpty(){

		assertTrue("Al principio la pila debe estar vacia", pila.isEmpty());
		
		setupEscenario1( );
		
		assertFalse("Ahora la pila no debe estar vacia", pila.isEmpty());
		
		setupEscenario2( );
		
		assertTrue("Ahora la pila debe estar vacia", pila.isEmpty());
	}

}

